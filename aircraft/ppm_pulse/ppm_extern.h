#ifndef _ppm_extern_H_
#define _ppm_extern_H_

extern struct ppm_pulse_t ppm_stat;
extern unsigned int now_cmd;
extern unsigned int count;
extern int ppm_nr;
extern const u32 *ppm_width_p[PPM_CMD_CNT+1];

#endif
