#include "ppm_pulse.h"
#include "ppm_extern.h"

#ifndef __DEBUG_PRINTK_TEST
irqreturn_t ppm_interrupt(int irq, void *dev_id) {
	if (count==0) {
		ppm_nr=_IOC_NR(now_cmd);
/*
 * NOTE:
 *   by reading asm/ioctl.h, know that _IOC_NRSHIFT is 0, so _IOC_NR() fetches 
 *   the original sequential number of corresponding command.
 */
	}
	if(count&0x1) {
		writel(readl(ppm_stat.gpio_base+PBDAT_OFFSET)&(~0x4),ppm_stat.gpio_base+PBDAT_OFFSET);
	} else {
		writel(readl(ppm_stat.gpio_base+PBDAT_OFFSET)|0x4,ppm_stat.gpio_base+PBDAT_OFFSET);
	}
	writel(ppm_width_p[ppm_nr][count],ppm_stat.timer_base+TINTVAL_OFFSET);
	writel(0x0, ppm_stat.timer_base+TCURR_OFFSET);
	writel(0x10,ppm_stat.timer_base+TIRQSTUS_OFFSET);
	writel(0xB7,ppm_stat.timer_base+TCON_OFFSET);
	count++;
	if(count==18) {
		count=0;
	//	if (now_cmd==PPM_START) now_cmd=PPM_STOP;
	}
	return IRQ_HANDLED;
}
#else
irqreturn_t ppm_interrupt(int irq, void *dev_id) {
	if (count==0)
		printk("Hello world!\n");
	writel(3000,ppm_stat.timer_base+TINTVAL_OFFSET);
	writel(0x0,ppm_stat.timer_base+TCURR_OFFSET);
	writel(0x10,ppm_stat.timer_base+TIRQSTUS_OFFSET);
	writel(0xB7,ppm_stat.timer_base+TCON_OFFSET);
	count++;
	if(count==1000)
		count=0;
	return IRQ_HANDLED;
}
#endif
