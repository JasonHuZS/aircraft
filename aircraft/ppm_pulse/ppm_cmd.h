#ifndef _ppm_cmd_H_
#define _ppm_cmd_H_
#include <linux/ioctl.h>

/****--INSTRUCTIONS SECTIONS--****/
#define PPM_MAGIC_TYPE	0x81
//remember to check this magic number
//checked 0x81 free to use
#define PPM_CMD_CNT	(10)

#define PPM_START	_IO(PPM_MAGIC_TYPE,0x1)
#define PPM_UP		_IO(PPM_MAGIC_TYPE,0x2)
#define PPM_DOWN	_IO(PPM_MAGIC_TYPE,0x3)
#define PPM_FORWARD	_IO(PPM_MAGIC_TYPE,0x4)
#define PPM_BACKWARD	_IO(PPM_MAGIC_TYPE,0x5)
#define PPM_LEFT	_IO(PPM_MAGIC_TYPE,0x6)
#define PPM_RIGHT	_IO(PPM_MAGIC_TYPE,0x7)
#define PPM_LEFT_ROT	_IO(PPM_MAGIC_TYPE,0x8)
#define PPM_RIGHT_ROT	_IO(PPM_MAGIC_TYPE,0x9)
#define PPM_STOP	_IO(PPM_MAGIC_TYPE,0xA)

#endif
