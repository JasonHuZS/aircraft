#ifndef _ppm_pulse_H_
#define _ppm_pulse_H_

/****--DEBUG SWITCH SECTION--****/
#define __DEBUG_IO_OUTPUT
#define __DEBUG_HAVE_INT
//#define __DEBUG_PRINTK_TEST

/****--HEAD FILE SECTION__****/
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/errno.h>
#include <linux/fs.h>
#include <linux/interrupt.h>
#include <linux/kdev_t.h>
#include <linux/kernel.h>
#include <linux/kobject.h>
#include <linux/module.h>
#include <linux/pwm.h>
#include <linux/ioctl.h>
#include <linux/sched.h>
#include <linux/sysfs.h>

#include <asm/atomic.h>
#include <asm/io.h>
#include <asm/delay.h>

#include <mach/platform.h>
#include <plat/system.h>
#include <plat/sys_config.h>

#include "ppm_cmd.h"

#define DEV_IRQ		(99)
#define DEV_NAME	"ppm_pulse"
#define DEV_CLS_NAME	"ppm_class"
#define DEV_DVC_NAME	"ppm%d"

//base: SW_PA_TIMERC_IO_BASE
#define TIMERC_RANGE	(0x100)
//Timer ctrl registers offset
#define TIRQEN_OFFSET	(0x0)
#define TIRQSTUS_OFFSET	(0x4)
#define TCON_OFFSET	(0x50)
#define TINTVAL_OFFSET	(0x54)
#define TCURR_OFFSET	(0x58)

//base: SW_PA_PORTC_IO_BASE
#define PORTC_RANGE	(0x100)
//GPIO ctrl register offset
#define PBCFG0_OFFSET	(1*0x24+0x0)
#define PBDAT_OFFSET	(1*0x24+0x10)

struct ppm_pulse_t{
	dev_t dev;
	void *timer_base;
	void *gpio_base;
	u32 TIRQEN;
	u32 TIRQSTUS;
	u32 TCON;
	u32 TINTVAL;
	u32 TCURR;
	u32 PBCFG0;
	u32 PBDAT;
	struct cdev cdev;	
	struct class *cls;
	struct device *dvc;
};

#endif
