#include "ppm_pulse.h"
#include "ppm_data.h"

extern irqreturn_t ppm_interrupt(int, void *);

static int ppm_init(void);
static void ppm_exit(void);
static int ppm_open(struct inode *, struct file *);
static int ppm_release(struct inode *, struct file *);
static long ppm_ioctl(struct file *, unsigned int, unsigned long);

/****--local functions--****/
static void __ppm_data_init(void);
static void __ppm_ioremap(void);
static void __ppm_iounmap(void);
static void __ppm_regs_backup(void);
static void __ppm_regs_recover(void);

struct file_operations ppm_ops={
	.owner	=THIS_MODULE,
	.open	=ppm_open,
	.release=ppm_release,
	.unlocked_ioctl	=ppm_ioctl,
};

static int __init ppm_init(void){
	int res;
	ppm_stat.cls=NULL;
	ppm_stat.dvc=NULL;
	res=alloc_chrdev_region(&ppm_stat.dev, 0, 1, DEV_NAME);
	if (res) {
		printk(KERN_WARNING "ppm pulse init failed!\n");
		return res;
	}
	cdev_init(&ppm_stat.cdev, &ppm_ops);
	ppm_stat.cdev.owner=THIS_MODULE;
	ppm_stat.cdev.ops=&ppm_ops;
	__ppm_ioremap();
	__ppm_regs_backup();
	res=cdev_add(&ppm_stat.cdev, ppm_stat.dev, 1);
	if (res){
		printk(KERN_WARNING "cdev_add error! errno: %d.\n", res);
		goto ppm_cdev_add_err;
	}
	
	ppm_stat.cls=class_create(THIS_MODULE, DEV_CLS_NAME);
	if (IS_ERR(ppm_stat.cls)) {
		printk(KERN_WARNING "cannot create class!\n");
		res=PTR_ERR(ppm_stat.cls);
		goto ppm_class_err;
	}

	ppm_stat.dvc=device_create(ppm_stat.cls, NULL, ppm_stat.dev, 
		NULL, DEV_DVC_NAME, 0);
	if (IS_ERR(ppm_stat.dvc)) {
		printk(KERN_WARNING "cannot create device!\n");
		res=PTR_ERR(ppm_stat.dvc);
		goto ppm_device_err;
	}
	return 0;

ppm_device_err:
	class_destroy(ppm_stat.cls);
ppm_class_err:
	cdev_del(&ppm_stat.cdev);
ppm_cdev_add_err:
	__ppm_regs_recover();
	unregister_chrdev_region(ppm_stat.dev,1);
	__ppm_iounmap();
	return res;
}

static void __exit ppm_exit(void) {
	device_destroy(ppm_stat.cls, ppm_stat.dev);
	class_destroy(ppm_stat.cls);
	cdev_del(&ppm_stat.cdev);
	__ppm_regs_recover();
	__ppm_iounmap();
	unregister_chrdev_region(ppm_stat.dev,1);
	return;
}

static atomic_t ppm_avail=ATOMIC_INIT(1);
static int ppm_open(struct inode * nodep, struct file *filp) {
	int ret;
	if (!atomic_dec_and_test(&ppm_avail)){
		atomic_inc(&ppm_avail);
		printk(KERN_WARNING "Device has been opened!\n");
		return -EBUSY;
	}
	__ppm_data_init();
	/*register modifications*/
	writel(0x10,ppm_stat.timer_base+TIRQSTUS_OFFSET);
	writel(0x0,ppm_stat.timer_base+TCURR_OFFSET);
	writel(0xB4,ppm_stat.timer_base+TCON_OFFSET);
	writel(ppm_width_p[ppm_nr][count],ppm_stat.timer_base+TINTVAL_OFFSET);
	writel(0xB5,ppm_stat.timer_base+TCON_OFFSET);
	writel(readl(ppm_stat.timer_base+TIRQEN_OFFSET)|0x10,ppm_stat.timer_base+TIRQEN_OFFSET);
#ifdef __DEBUG_IO_OUTPUT
	writel(readl(ppm_stat.gpio_base+PBCFG0_OFFSET)|0x100,ppm_stat.gpio_base+PBCFG0_OFFSET);
	writel(readl(ppm_stat.gpio_base+PBDAT_OFFSET)|0x4,ppm_stat.gpio_base+PBDAT_OFFSET);
#endif
	count++;
	
#ifdef __DEBUG_HAVE_INT
	ret=request_irq(DEV_IRQ, ppm_interrupt, 0, DEV_NAME, NULL);
	if (ret) {
		printk(KERN_WARNING "cannot install interrupt!\n");
		__ppm_regs_recover();
		return ret;
	}
#endif
	return 0;
}

static int ppm_release(struct inode * nodep, struct file *filp) {
#ifdef __DEBUG_HAVE_INT
	free_irq(DEV_IRQ, NULL);
#endif
	__ppm_regs_recover();
	atomic_inc(&ppm_avail);
	return 0;
}
static long ppm_ioctl(struct file *filp, unsigned int cmd, unsigned long args) {
	switch (cmd) {
	case PPM_START: break;
	case PPM_UP: break;
	case PPM_DOWN: break;
	case PPM_FORWARD: break;
	case PPM_BACKWARD: break;
	case PPM_LEFT: break;
	case PPM_RIGHT: break;
	case PPM_LEFT_ROT: break;
	case PPM_RIGHT_ROT: break;
	case PPM_STOP: break;
	default:
		printk(KERN_WARNING "invalid command!\n");
		return -EINVAL;
	}
	now_cmd=cmd;
	return 0;
}

/***************--local function implementations--*******************/
static void __ppm_data_init() {
	now_cmd=PPM_STOP;
	ppm_width_p[0]=NULL;
	ppm_width_p[1]=ppm_witdh_start;
	ppm_width_p[2]=ppm_width_up;
	ppm_width_p[3]=ppm_width_down;
	ppm_width_p[4]=ppm_width_forward;
	ppm_width_p[5]=ppm_width_backward;
	ppm_width_p[6]=ppm_width_left;
	ppm_width_p[7]=ppm_width_right;
	ppm_width_p[8]=ppm_width_left_rot;
	ppm_width_p[9]=ppm_width_right_rot;
	ppm_width_p[10]=ppm_width_stop;
	ppm_nr=10;
	count=0;
	return;
}

static void __ppm_ioremap() {
	ppm_stat.timer_base= ioremap(SW_PA_TIMERC_IO_BASE, TIMERC_RANGE);
	ppm_stat.gpio_base = ioremap(SW_PA_PORTC_IO_BASE,  PORTC_RANGE);
	return;
}
void __ppm_iounmap() {
	iounmap(ppm_stat.timer_base);
	iounmap(ppm_stat.gpio_base);
	return;
}
static void __ppm_regs_backup() {
	ppm_stat.TIRQEN		=readl(ppm_stat.timer_base+TIRQEN_OFFSET);
	ppm_stat.TIRQSTUS	=readl(ppm_stat.timer_base+TIRQSTUS_OFFSET);
	ppm_stat.TCON		=readl(ppm_stat.timer_base+TCON_OFFSET);
	ppm_stat.TINTVAL	=readl(ppm_stat.timer_base+TINTVAL_OFFSET);
	ppm_stat.TCURR		=readl(ppm_stat.timer_base+TCURR_OFFSET);
#ifdef __DEBUG_IO_OUTPUT
	ppm_stat.PBCFG0		=readl(ppm_stat.gpio_base+PBCFG0_OFFSET);
	ppm_stat.PBDAT		=readl(ppm_stat.gpio_base+PBDAT_OFFSET);
#endif
}
static void __ppm_regs_recover() {
#ifdef __DEBUG_IO_OUTPUT
	writel(ppm_stat.PBCFG0	,ppm_stat.gpio_base+PBCFG0_OFFSET);
	writel(ppm_stat.PBDAT	,ppm_stat.gpio_base+PBDAT_OFFSET);
#endif
	writel(ppm_stat.TCURR	,ppm_stat.timer_base+TCURR_OFFSET);
	writel(ppm_stat.TINTVAL	,ppm_stat.timer_base+TINTVAL_OFFSET);
	writel(ppm_stat.TCON	,ppm_stat.timer_base+TCON_OFFSET);
	writel(ppm_stat.TIRQSTUS,ppm_stat.timer_base+TIRQSTUS_OFFSET);
	writel(ppm_stat.TIRQEN	,ppm_stat.timer_base+TIRQEN_OFFSET);
}

module_init(ppm_init);
module_exit(ppm_exit);
MODULE_AUTHOR("HuStmpHrrr");
MODULE_LICENSE("Dual BSD/GPL");
MODULE_DESCRIPTION("control ppm pulse using timer interrupts");
