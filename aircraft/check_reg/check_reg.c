#include <linux/device.h>
#include <linux/sysfs.h>
#include <linux/fs.h>
#include <asm/io.h>
#include <asm/delay.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <mach/platform.h>
#include <linux/pwm.h>
#include <linux/kdev_t.h>
#include <linux/kobject.h>
#include <plat/system.h>
#include <plat/sys_config.h>

#include "check_reg.h"

//static int check_reg_init();
//static void check_reg_exit();

static check_reg_info_t info;

static int __init check_reg_init(void){
	int i;
	unsigned int tmp;
	info.length=0x80;
	info.base=ioremap(SW_PA_TIMERC_IO_BASE,info.length);
	printk("base addr: 0x%x:\n",(unsigned int)SW_PA_TIMERC_IO_BASE);
	printk("offset\tvalue\n");
	for (i=0;i<info.length;i+=4){
		printk("0x%2x:\t",(unsigned int)i);
		tmp=readl(info.base+i);
		printk("0x%8x\n",tmp);
	}
	return 0;
}
static void __exit check_reg_exit(void){
	return;
}

module_init(check_reg_init);
module_exit(check_reg_exit);
MODULE_LICENSE("GPL");
MODULE_AUTHOR("HuStmpHrrr");
MODULE_DESCRIPTION("check the values of timer control registers");
