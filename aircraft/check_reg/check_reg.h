#ifndef _check_reg_H_
#define _check_reg_H_

struct check_reg_info {
	void * base;
	unsigned long length;
};
typedef struct check_reg_info check_reg_info_t;

#endif
