#ifndef _iomain_H_
#define _iomain_H_

#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/errno.h>
#include <linux/fs.h>
#include <linux/interrupt.h>
#include <linux/kdev_t.h>
#include <linux/kernel.h>
#include <linux/kobject.h>
#include <linux/module.h>
#include <linux/pwm.h>
#include <linux/ioctl.h>
#include <linux/sched.h>
#include <linux/sysfs.h>

#include <asm/atomic.h>
#include <asm/io.h>
#include <asm/delay.h>

#include <mach/platform.h>
#include <plat/system.h>
#include <plat/sys_config.h>

#define DEV_NAME	"ioc"
#define DEV_CLS_NAME	"io_class"
#define DEV_DVC_NAME	"io%d"

//base: SW_PA_PORTC_IO_BASE
#define IO_RANGE	(0x100)
#define CFG_OFFSET	(3*0x24+0x0)
#define DAT_OFFSET	(3*0x24+0x10)

struct ioctrl_t {
	dev_t dev;
	void *base;
	u32 CFG;
	u32 DAT;
	struct cdev cdev;
	struct class *class;
	struct device *device;
};

#endif
