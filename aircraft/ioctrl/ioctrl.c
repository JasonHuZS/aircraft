#include "ioctrl.h"
#include "iocmd.h"

static int io_init(void);
static void io_exit(void);
static int io_open(struct inode *, struct file *);
static int io_release(struct inode *, struct file *);
static long io_ioctl(struct file *, unsigned int, unsigned long);

struct file_operations io_ops={
	.owner	=THIS_MODULE,
	.open	=io_open,
	.release=io_release,
	.unlocked_ioctl=io_ioctl,
};
struct ioctrl_t io_stat;

static int __init io_init(void) {
	int ret;
	io_stat.class=NULL;
	io_stat.device=NULL;
	ret=alloc_chrdev_region(&io_stat.dev, 0, 1, DEV_NAME);
	if (ret) {
		printk(KERN_WARNING "dev No. fetching failed!\n");
		return ret;
	}
	cdev_init(&io_stat.cdev, &io_ops);
	io_stat.cdev.owner=THIS_MODULE;
	io_stat.cdev.ops=&io_ops;

	io_stat.base=ioremap(SW_PA_PORTC_IO_BASE,IO_RANGE);
	io_stat.CFG=readl(io_stat.base+CFG_OFFSET);
	io_stat.DAT=readl(io_stat.base+DAT_OFFSET);
	
	ret=cdev_add(&io_stat.cdev, io_stat.dev, 1);
	if (ret){
		printk(KERN_WARNING "cdev_add error! errno: %d.\n", -ret);
		goto io_cdev_add_err;
	}
	
	io_stat.class=class_create(THIS_MODULE, DEV_CLS_NAME);
	if (IS_ERR(io_stat.class)) {
		printk(KERN_WARNING "cannot create class!\n");
		ret=PTR_ERR(io_stat.class);
		goto io_class_err;
	}

	io_stat.device=device_create(io_stat.class, NULL, io_stat.dev, 
		NULL, DEV_DVC_NAME, 0);
	if (IS_ERR(io_stat.device)) {
		printk(KERN_WARNING "cannot create device!\n");
		ret=PTR_ERR(io_stat.device);
		goto io_device_err;
	}
	return 0;

io_device_err:
	class_destroy(io_stat.class);
io_class_err:
	cdev_del(&io_stat.cdev);
io_cdev_add_err:
	writel(io_stat.CFG,io_stat.base+CFG_OFFSET);
	writel(io_stat.DAT,io_stat.base+DAT_OFFSET);
	iounmap(io_stat.base);
	unregister_chrdev_region(io_stat.dev,1);
	return ret;
}

static void __exit io_exit(void) {
	device_destroy(io_stat.class, io_stat.dev);
	class_destroy(io_stat.class);
	cdev_del(&io_stat.cdev);
	writel(io_stat.CFG,io_stat.base+CFG_OFFSET);
	writel(io_stat.DAT,io_stat.base+DAT_OFFSET);
	iounmap(io_stat.base);
	unregister_chrdev_region(io_stat.dev,1);
	return;
}

static atomic_t io_avail=ATOMIC_INIT(1);
static int io_open(struct inode * nodep, struct file *filp) {
	if (!atomic_dec_and_test(&io_avail)){
		atomic_inc(&io_avail);
		printk(KERN_WARNING "Device has been opened!\n");
		return -EBUSY;
	}
	writel((io_stat.DAT&(~0xf))|10,io_stat.base+DAT_OFFSET);
	writel(0x1111,io_stat.base+CFG_OFFSET);
	return 0;
}

static int io_release(struct inode * nodep, struct file *filp) {
	atomic_inc(&io_avail);
	return 0;
}
static long io_ioctl(struct file *filp, unsigned int cmd, unsigned long args) {
	unsigned int nr=_IOC_NR(cmd);
	if (nr>0)
		writel((readl(io_stat.base+DAT_OFFSET)&(~0xf))|nr,io_stat.base+DAT_OFFSET);
	else 
		printk(KERN_WARNING "invalid command!\n");
	return 0;
}

module_init(io_init);
module_exit(io_exit);
MODULE_AUTHOR("HuStmpHrrr");
MODULE_LICENSE("Dual BSD/GPL");
MODULE_DESCRIPTION("control io pulse using timer interrupts");
