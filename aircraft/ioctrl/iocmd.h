#ifndef _iocmd_H_
#define _iocmd_H_
#include <linux/ioctl.h>

/****--INSTRUCTIONS SECTIONS--****/
#define IO_MAGIC_TYPE	0x81
//remember to check this magic number
//checked 0x81 free to use
#define IO_CMD_CNT	(15)

#define IO_START	_IO(IO_MAGIC_TYPE,0x1)
#define IO_UP		_IO(IO_MAGIC_TYPE,0x2)
#define IO_DOWN		_IO(IO_MAGIC_TYPE,0x3)
#define IO_FORWARD	_IO(IO_MAGIC_TYPE,0x4)
#define IO_BACKWARD	_IO(IO_MAGIC_TYPE,0x5)
#define IO_LEFT		_IO(IO_MAGIC_TYPE,0x6)
#define IO_RIGHT	_IO(IO_MAGIC_TYPE,0x7)
#define IO_LEFT_ROT	_IO(IO_MAGIC_TYPE,0x8)
#define IO_RIGHT_ROT	_IO(IO_MAGIC_TYPE,0x9)
#define IO_STOP		_IO(IO_MAGIC_TYPE,0xA)
#define IO_RESV_1	_IO(IO_MAGIC_TYPE,0xB)
#define IO_RESV_2	_IO(IO_MAGIC_TYPE,0xC)
#define IO_RESV_3	_IO(IO_MAGIC_TYPE,0xD)
#define IO_OUT_ON	_IO(IO_MAGIC_TYPE,0xE)
#define IO_OUT_OFF	_IO(IO_MAGIC_TYPE,0xF)
#define IO_PAUSE	IO_RESV_1

#endif
