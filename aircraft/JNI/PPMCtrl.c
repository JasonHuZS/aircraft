#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <PPMCtrl.h>
#include <iocmd.h>
#include <sys/ioctl.h>

static int fd;
static unsigned int cmd_arr[IO_CMD_CNT+1]={
	0,IO_START,IO_UP,IO_DOWN,IO_FORWARD,IO_BACKWARD,
	IO_LEFT,IO_RIGHT,IO_LEFT_ROT,IO_RIGHT_ROT,IO_STOP,
	IO_RESV_1,IO_RESV_2,IO_RESV_3,IO_OUT_ON,IO_OUT_OFF,
};

JNIEXPORT jboolean JNICALL Java_PPMCtrl_init (JNIEnv *env, jclass clz) {
	int ret;
	fd=-1;
	fd=open("/dev/io0",O_RDWR);
	if (fd==-1){
		perror("cannot open device file!");
		return JNI_FALSE;
	}
	ret=ioctl(fd, cmd_arr[14]);
	if (ret) {
		perror("command error!");
		return JNI_FALSE;
	}
	return JNI_TRUE;
}

JNIEXPORT void JNICALL Java_PPMCtrl_exit (JNIEnv *env, jclass clz) {
	ioctl(fd, cmd_arr[1]);
	close(fd);
}

JNIEXPORT jboolean JNICALL Java_PPMCtrl_sendCmd (JNIEnv *env, jclass clz, jint cmd) {
	int ret;
	printf("%d\n",cmd);
	ret=ioctl(fd, cmd_arr[cmd]);
	if (ret) {
		perror("command error!");
		return JNI_FALSE;
	}
	return JNI_TRUE;
}
