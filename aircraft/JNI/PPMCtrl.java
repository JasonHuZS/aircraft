public class PPMCtrl {
	public static native boolean init();
	public static native void exit();
	public static native boolean sendCmd(int cmd);

	static {
		System.loadLibrary("ioctrl");
	}

}
