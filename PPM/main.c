#define __Alternative_TimerA_0_

#include "msp430f2619.h"
#include "TimerA.h"
#include "General.h"
#include "SysClk.h"

#define START     (1)
#define UP        (2)
#define DOWN      (3)
#define FORWARD   (4)
#define BACKWARD  (5)
#define LEFT      (6)
#define RIGHT     (7)
#define LEFT_ROT  (8)
#define RIGHT_ROT (9)
#define STOP      (10)
#define OUT_ON    (14)
#define OUT_OFF   (15)

/*
 * USE P5 TO RECIEVE DATA!!!!
 * FROM 0 TO 3!!!!!
 */
#define PERIOD    (20)

void init();
//int state;
int count;
int ocount;
//unsigned int *iterp[11];
const unsigned int start[9]={4000,4000,4000,
  4000,4000,7400,7400,7400,37800};
/*unsigned int up[9]={6000,6000,6500,
  6000,4000,7400,7400,7400,29300};
unsigned int down[9]={6000,6000,5500,
  6000,4000,7400,7400,7400,30300};
unsigned int forward[9]={6000,6500,6000,
  6000,4000,7400,7400,7400,29300};
unsigned int backward[9]={6000,5500,6000,
  6000,4000,7400,7400,7400,30300};
unsigned int left[9]={5500,6000,6000,
  6000,4000,7400,7400,7400,30300};
unsigned int right[9]={6500,6000,6000,
  6000,4000,7400,7400,7400,29300};
unsigned int left_rot[9]={6000,6000,6000,
  5500,4000,7400,7400,7400,30300};
unsigned int right_rot[9]={6000,6000,6000,
  6500,4000,7400,7400,7400,29300};*/
const unsigned int stop[9]={6000,6000,6000,
  6000,4000,7400,7400,7400,29800};
unsigned int modi[9]={6000,6000,6000,
  6000,4000,7400,7400,7400,29800};

int main( void )
{
  // Stop watchdog timer to prevent time out reset
  WDTCTL = WDTPW + WDTHOLD;
  
  init();
  TIMERA_START;
  
  __bis_SR_register(CPUOFF+GIE);
  
  return 0;
}

void init() {
  SYSCLK_MCLK_DCO_16MHZ(1);
  SYSCLK_SCLK_DCO(4);
  
  P1DIR=0xff;
  P1OUT=0xff;
  
  TimerACompareInit(TIMERA_UP,2,
                    4000,TIMERA_OUT,
                    1600,TIMERA_RS);
  TIMERA_SET_CC0IE;
  count=0;
  ocount=0;
  
  CLR_BIT(P5DIR,0xf);
}

#pragma vector=TIMERA0_VECTOR
__interrupt void TimerA0ISR(){
  unsigned int ins;
  _DINT();
  TIMERA_CCR0_ASN(modi[count]);
  count++;
  if(count==9){
    count=0;
    ocount++;
    if (ocount==PERIOD) {
      ocount=0;
      ins=P5IN&0xf;
      switch(ins) {
      case OUT_ON:
        TIMERA_TA_INIT(P2,BIT3);
        break;
      case OUT_OFF:
        CLR_BIT((P2DIR),(BIT3));
        CLR_BIT((P2SEL),(BIT3));
        break;
      case START:
        modi[0]=start[0];
        modi[1]=start[1];
        modi[2]=start[2];
        modi[3]=start[3];
        modi[4]=start[4];
        modi[5]=start[5];
        modi[6]=start[6];
        modi[7]=start[7];
        modi[8]=start[8];
        break;
      case STOP:
        modi[0]=stop[0];
        modi[1]=stop[1];
        modi[2]=stop[2];
        modi[3]=stop[3];
        modi[4]=stop[4];
        modi[5]=stop[5];
        modi[6]=stop[6];
        modi[7]=stop[7];
        modi[8]=stop[8];
        break;
      case UP:
        modi[2]+=200;
        modi[8]-=200;
        break;
      case DOWN:
        modi[2]-=200;
        modi[8]+=200;
        break;
      case FORWARD:
        modi[1]+=200;
        modi[8]-=200;
        break;
      case BACKWARD:
        modi[1]-=200;
        modi[8]+=200;
        break;
      case RIGHT:
        modi[0]+=200;
        modi[8]-=200;
        break;
      case LEFT:
        modi[0]-=200;
        modi[8]+=200;
        break;
      case RIGHT_ROT:
        modi[3]+=200;
        modi[8]-=200;
        break;
      case LEFT_ROT:
        modi[3]-=200;
        modi[8]+=200;
        break;
      default:
      }
    }
  }
  __bis_SR_register_on_exit(GIE);
}